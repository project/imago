/**
 * @file
 * Defines the behavior of the media entity browser view.
 */

(function ($, _, Backbone, Drupal, drupalSettings) {

  'use strict';

  var Selection = Backbone.View.extend({

    events: {
      'click .views-row': 'onClick',
      'dblclick .views-row': 'onClick',
      'click .views-exposed-form': 'onClick',
      'click .imago-load-more': 'onClick'
    },

    // Display selected items counter.
    renderCounter: function () {
      // Remove existing status messages, so we can add new status messages.
      $('.media-browser-file-counter').remove();
    },

    initialize: function () {
      // This view must be created on an element which has this attribute.
      // Otherwise, things will blow up and rightfully so.
      this.uuid = this.el.getAttribute('data-entity-browser-uuid');

      // If we're in an iFrame, reach into the parent window context to get the
      // settings for this entity browser.
      var settings = (frameElement ? parent : window).drupalSettings.entity_browser[this.uuid];
      var max_cardinality = drupalSettings.max_cardinality;
      var prev_selected = drupalSettings.prev_selected_count;
      if (prev_selected !== undefined) {
        max_cardinality = drupalSettings.max_cardinality - prev_selected;
      }
      var cardinality = settings.cardinality || 1;
      if (cardinality < 0) {
        cardinality = max_cardinality;
      }
      $('.imago-load-more').prop('disabled', false);

      // scroll to bottom after click on load more
      var load_more = drupalSettings.load_more;
      if (load_more !== undefined && load_more) {
        $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
      }

      // Assume a single-cardinality field with no existing selection.
      this.count = settings.count || 0;
      if (prev_selected === drupalSettings.max_cardinality) {
        var prev_search = drupalSettings.prev_search;
        var caption = $('[id^=edit-caption]').val();
        if (prev_search == caption) {
          this.count = prev_selected;
          this.lock();
        }
        else {
          cardinality = drupalSettings.max_cardinality;
        }
      }
      this.cardinality = cardinality;
    },

    deselect: function (item) {
      this.$(item)
          .removeClass('checked')
          .find('input[name ^= "imago_browser_select"]')
          .prop('checked', false);
    },

    /**
     * Deselects all items in the entity browser.
     */
    deselectAll: function () {
      // Create a version of deselect() that can be called within each() with
      // this as its context.
      var _deselect = jQuery.proxy(this.deselect, this);

      this.$('.views-row').each(function (undefined, item) {
        _deselect(item);
      });
    },

    select: function (item) {
      this.$(item)
          .addClass('checked')
          .find('input[name ^= "imago_browser_select"]')
          .prop('checked', true);
    },

    /**
     * Marks unselected items in the entity browser as disabled.
     */
    lock: function () {
      this.$('.views-row:not(.checked)').addClass('disabled');
    },

    /**
     * Marks all items in the entity browser as enabled.
     */
    unlock: function () {
      this.$('.views-row').removeClass('disabled');
    },

    /**
     * Warn the user with a specific message based on the error type.
     * @param {string} errorType - The type of error ('limit' or 'broken').
     */
    warn: function(errorType) {
      var text;
      if (errorType === 'broken') {
        text = Drupal.t('The selected image is broken and cannot be imported.');
      } else {
        text = Drupal.t('You can import a maximum of @count images at a time.', {'@count': drupalSettings.max_cardinality});
      }
      var $message = $('<span class="media-browser-limit"></span>').text(' ' + text);
      alert(text);
      if (!this.$('.media-browser-file-counter .media-browser-limit').length) {
        this.$('.media-browser-file-counter').addClass('messages--warning').append($message);
      }
    },

    /**
     * Remove limit warning message.
     */
    unwarn: function () {
      this.$('.media-browser-limit').each(function () {
        $(this).remove();
      });
      this.$('.media-browser-file-counter').removeClass('messages--warning').addClass('messages--status');
    },

    /**
     * Handles click events for any item in the entity browser.
     *
     * @param {jQuery.Event} event
     */
    onClick: function (event) {
      var chosen_one = this.$(event.currentTarget);

      if (chosen_one.hasClass('views-exposed-form')) {
        var prev_selected_count = drupalSettings.prev_selected_count;
        if (prev_selected_count !== undefined) {
          var all_count = prev_selected_count + this.count;
          if (all_count >= this.cardinality) {
            this.lock();
          }
        }
      } else if (chosen_one.hasClass('imago-load-more')) {
        chosen_one = $('.views-row.checked');
        this.deselectAll();
        this.select(chosen_one);
        $(this).prop('disabled', true);
      } else if (chosen_one.hasClass('disabled')) {
        this.warn('broken');
        return false;
      } else {
        var img = chosen_one.find('img')[0];
        if (img) {
          var self = this;
          isImageBroken(img.src, function(isBroken) {
            if (isBroken) {
              self.warn('broken');
            } else {
              self.handleSelection(chosen_one);
            }
          });
        } else {
          self.handleSelection(chosen_one);
        }
      }
    },

    /**
     * Handle the selection logic for an item.
     * @param {jQuery} item - The item to handle.
     */
    handleSelection: function (item) {
      if (this.cardinality === 1) {
        this.deselectAll();
        this.select(item);
      } else if (item.hasClass('checked')) {
        this.deselect(item);
        this.count--;
        this.unlock();
        this.unwarn();
        this.renderCounter();
      } else {
        if (this.count >= this.cardinality) {
          this.warn('limit');
        } else {
          this.select(item);
          this.count++;
          if (this.count === this.cardinality) {
            this.lock();
          }
          this.renderCounter();
        }
      }
    }

  });

  /**
   * Check if an image is broken.
   * @param {string} url - The URL of the image.
   * @param {function} callback - The callback function to call with the result.
   */
  function isImageBroken(url, callback) {
    var image = new Image();
    image.onload = function() {
      callback(false);
    };
    image.onerror = function() {
      callback(true);
    };
    image.src = url;
  }

  /**
   * Attaches the behavior of the media entity browser view.
   */
  Drupal.behaviors.mediaEntityBrowserView = {
    getElement: function (context) {
      // If we're in a document context, search for the first available entity
      // browser form. Otherwise, ensure that the context is itself an entity
      // browser form.
      return $(context)[context === document ? 'find' : 'filter']('form[data-entity-browser-uuid]').get(0);
    },

    attach: function (context) {
      var element = this.getElement(context);
      if (element) {
        $(element).data('view', new Selection({ el: element }));

        // Check for broken images and disable selection if broken.
        $('.views-row img', element).each(function () {
          var imgElement = $(this);
          var url = imgElement.attr('src');
          isImageBroken(url, function (isBroken) {
            if (isBroken) {
              imgElement.closest('.views-row').addClass('disabled');
              imgElement.replaceWith('<div class="disabled-image">' + Drupal.t('Image could not be uploaded.') + '</div>');
            }
          });
        });
      }
    },

    detach: function (context) {
      var element = this.getElement(context);

      if (element) {
        var view = $(element).data('view');

        if (view instanceof Selection) {
          view.undelegateEvents();
        }
      }
    }
  };

  /**
   * Auto select images from previous page.
   */
  Drupal.behaviors.selectedImages = {
    attach: function attach(context) {
      var selected_images = drupalSettings.selected_images;
      $.each( selected_images, function( key, value) {
        if (value != null) {
          var imageId = value.replace(":", "");
          var image_selector = $('#edit-imago-browser-select-'+imageId);
          var viewRow = $('.views-row');
          var imageInput = $(viewRow).find('#edit-imago-browser-select-'+imageId);
          if (viewRow.length > 0) {
            $(image_selector).prop('checked', true);
            $(image_selector).closest('.views-row').removeClass('disabled');
            $(image_selector).closest('.views-row').addClass('checked autochecked');
          }
        }
      });
    }
  }

  /**
   * Registers behaviours related to view widget.
   */
  Drupal.behaviors.entityBrowserView = {
    attach: function (context) {
      // Add the AJAX to exposed forms.
      // We do this as the selector in core/modules/views/js/ajax_view.js
      // assumes that the form the exposed filters reside in has a
      // views-related ID, which ours does not.
      var views_instance = Drupal.views.instances[Object.keys(Drupal.views.instances)[0]];
      if (views_instance) {
        views_instance.$exposed_form = $('.js-view-dom-id-' + views_instance.settings.view_dom_id + ' .views-exposed-form');
        $(once('exposed-form', views_instance.$exposed_form)).each(jQuery.proxy(views_instance.attachExposedFormAjax, views_instance));

        // The form values form_id, form_token, and form_build_id will break
        // the exposed form. Remove them by splicing the end of form_values.
        if (views_instance.exposedFormAjax && views_instance.exposedFormAjax.length > 0) {
          var ajax = views_instance.exposedFormAjax[0];
          ajax.options.beforeSubmit = function (form_values, element_settings, options) {
            form_values = form_values.splice(form_values.length - 3, 3);
            ajax.ajaxing = true;
            return ajax.beforeSubmit(form_values, element_settings, options);
          };
        }

        // Handle Enter key press in the views exposed form text fields: ensure
        // that the correct button is used for the views exposed form submit.
        // The default browser behavior for the Enter key press is to click the
        // first found button. But there can be other buttons in the form, for
        // example, ones added by the Tabs widget selector plugin.
        $(once('submit-by-enter-key', views_instance.$exposed_form)).find('input[type="text"]').each(function () {          $(this).on('keypress', function (event) {
            if (event.keyCode == 13) {
              event.preventDefault();
              views_instance.$exposed_form.find('input[type="submit"]').first().click();
            }
          });
        });

        var select = views_instance.$exposed_form.find('.form-type-select');
        $(select).on('change', function () {
          views_instance.$exposed_form.find('input[type="submit"]').first().click();
        });

        // Display / hide buttons if no results was found.
        if (views_instance.$view.find('div.view-empty').length > 0) {
          $('.imago-load-more').hide();
          $('.is-entity-browser-submit.button').hide();
        }
        else {
          $('.imago-load-more').show();
          $('.is-entity-browser-submit.button').show();
        }
      }
    }
  };

})(jQuery, _, Backbone, Drupal, drupalSettings);