<?php

namespace Drupal\imago\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_browser\DisplayAjaxInterface;
use Drupal\entity_browser\Form\EntityBrowserForm;

/**
 * The entity browser form.
 */
class ImageBrowserForm extends EntityBrowserForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'imago_browser_' . $this->entityBrowser->id() . '_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'imago_browser_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $original_widget = $this->getCurrentWidget($form_state);
    if ($new_widget = $this->entityBrowser->getWidgetSelector()->submit($form, $form_state)) {
      $this->setCurrentWidget($new_widget, $form_state);
    }

    // Only call widget submit if we didn't change the widget.
    if ($original_widget == $this->getCurrentWidget($form_state)) {
      $this->entityBrowser
        ->getWidgets()
        ->get($this->getCurrentWidget($form_state))
        ->submit($form[$form['#browser_parts']['widget']], $form, $form_state);

      $this->entityBrowser
        ->getSelectionDisplay()
        ->submit($form, $form_state);
    }

    if (!$this->isSelectionCompleted($form_state)) {
      $form_state->setRebuild();
    }
    else {
      $this->entityBrowser->getDisplay()->selectionCompleted($this->getSelectedEntities($form_state));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // During the initial form build, add this form object to the form state and
    // allow for initial preparation before form building and processing.
    if (!$form_state->has('entity_form_initialized')) {
      $this->init($form_state);
    }

    $this->isFunctionalForm();

    $form['#attributes']['class'][] = 'entity-browser-form';
    if (!empty($form_state->get(['entity_browser', 'instance_uuid']))) {
      $form['#attributes']['data-entity-browser-uuid'] = $form_state->get(['entity_browser', 'instance_uuid']);
    }
    $form['#browser_parts'] = [
      'widget_selector' => 'widget_selector',
      'widget' => 'widget',
      'selection_display' => 'selection_display',
    ];

    if (!($current_widget_id = $this->getCurrentWidget($form_state))) {
      $this->messenger->addWarning($this->t('No widgets are available.'));
      return $form;
    }

    $this->entityBrowser
      ->getWidgetSelector()
      ->setDefaultWidget($current_widget_id);
    $form[$form['#browser_parts']['widget_selector']] = $this->entityBrowser
      ->getWidgetSelector()
      ->getForm($form, $form_state);

    $widget = $this->entityBrowser->getWidget($current_widget_id);
    if ($widget->access()->isAllowed()) {
      $form[$form['#browser_parts']['widget']] = $widget->getForm($form, $form_state, $this->entityBrowser->getAdditionalWidgetParameters());
    }
    else {
      $this->messenger->addWarning($this->t('Access to the widget forbidden.'));
    }

    // Add cache access cache metadata from the widgets to the form directly as
    // it is affected.
    foreach ($this->entityBrowser->getWidgets() as $widget) {
      /** @var \Drupal\entity_browser\WidgetInterface $widget */
      $this->renderer->addCacheableDependency($form, $widget->access());
    }

    $form[$form['#browser_parts']['selection_display']] = $this->entityBrowser
      ->getSelectionDisplay()
      ->getForm($form, $form_state);

    if ($this->entityBrowser->getDisplay() instanceof DisplayAjaxInterface) {
      $this->entityBrowser->getDisplay()->addAjax($form);
    }

    $form['#attached']['library'][] = 'entity_browser/entity_browser';

    $form['#disable_inline_form_errors'] = TRUE;

    return $form;
  }

}
