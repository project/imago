<?php

namespace Drupal\imago\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for the Article candidate module.
 */
class ImagoSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['imago.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'imago_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('imago.settings')->getRawData();

    $form = [
      '#type' => 'details',
      '#title' => t('Imago API settings.', [], ['context' => 'imago']),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API settings'),
      '#description' => $this->t('The name used to indicate anonymous users.'),
    ];
    $form['api']['api_url'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Search'),
    ];
    $form['api']['api_url']['base_url'] = [
      '#title' => $this->t('Base API URL.', [], ['context' => 'imago']),
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => ($config['api']['api_url']['base_url']) ?? NULL,
      '#description' => $this->t('The base API URL.'),
      '#placeholder' => 'https://API-URL.imago-images.de',
      '#required' => TRUE,
    ];
    $form['api']['api_url']['search_url'] = [
      '#title' => $this->t('Search API URL.', [], ['context' => 'imago']),
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => ($config['api']['api_url']['search_url']) ?? NULL,
      '#description' => $this->t('Full API url for search service. Replace API-URL by your credentials.'),
      '#placeholder' => 'https://API-URL.imago-images.de/search',
      '#required' => TRUE,
    ];
    $form['api']['api_url']['download_url'] = [
      '#title' => $this->t('Download API URL.', [], ['context' => 'imago']),
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => ($config['api']['api_url']['download_url']) ?? NULL,
      '#description' => $this->t('Full API url for download. Replace API-URL by your credentials.'),
      '#placeholder' => 'https://API-URL.imago-images.de/download',
      '#required' => TRUE,
    ];
    $form['api']['api_user'] = [
      '#title' => $this->t('API user.', [], ['context' => 'imago']),
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => ($config['api']['api_user']) ?? NULL,
      '#description' => $this->t('Your Imago API-USER-NAME.'),
      '#required' => TRUE,
    ];
    $required_key = FALSE;
    if (empty($config['api']['api_key'])) {
      $required_key = TRUE;
    }
    $form['api']['api_key'] = [
      '#title' => $this->t('API key', [], ['context' => 'imago']),
      '#type' => 'password',
      '#size' => 60,
      '#default_value' => ($config['api']['api_key']) ?? NULL,
      '#description' => $this->t('Your Imago API-KEY.'),
      '#required' => $required_key,
    ];
    $media_types = $types = \Drupal::entityTypeManager()->getStorage('media_type')->loadMultiple();
    $options = [];
    foreach ($media_types as $media_type) {
      $options[$media_type->id()] = $media_type->label();
    }
    $form['media_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination media bundle'),
      '#options' => $options,
      '#default_value' => ($config['media_bundle']) ?? NULL,
      '#description' => $this->t('Media bundle for imported images.'),
      '#required' => TRUE,
    ];

    $image_fields = [];
    $integer_fields = [];
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions('media', $config['media_bundle']);
    foreach ($fields as $field) {
      if ($field->getType() === 'image' )  {
        $image_fields[$field->getName()] = $field->getName();
      }
      if ($field->getType() === 'integer' )  {
        $integer_fields[$field->getName()] = $field->getName();
      }
      if ($field->getType() === 'string' || $field->getType() === 'text_long')  {
        $text_fields[$field->getName()] = $field->getName();
      }
    }
    $form['image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#options' => $image_fields,
      "#empty_option"=>t('- Select -'),
      '#default_value' => ($config['image_field']) ?? NULL,
      '#description' => $this->t('Image field, where will be file stored.'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="media"]' =>['!value' => ''],
        ],
      ],
    ];

    $form['imago_id_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Imago ID field'),
      '#options' => $integer_fields,
      "#empty_option"=>t('- Select -'),
      '#default_value' => ($config['imago_id_field']) ?? NULL,
      '#description' => $this->t('Field for imago ID.'),
      '#states' => [
        'visible' => [
          ':input[name="media"]' =>['!value' => ''],
        ],
      ],
    ];

    $form['height_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field for height'),
      "#empty_option"=>t('- Select -'),
      '#options' => $integer_fields,
      '#default_value' => ($config['height_field']) ?? NULL,
      '#description' => $this->t('Field for storing image height'),
      '#states' => [
        'visible' => [
          ':input[name="media"]' =>['!value' => ''],
        ],
      ],
    ];

    $form['width_field'] = [
      '#type' => 'select',
      '#title' =>$this->t('Field for width'),
      "#empty_option"=>t('- Select -'),
      '#options' => $integer_fields,
      '#default_value' => ($config['width_field']) ?? NULL,
      '#description' => $this->t('Field for storing image width'),
      '#states' => [
        'visible' => [
          ':input[name="media"]' =>['!value' => ''],
        ],
      ],
    ];

    $form['media_source'] = [
      '#type' => 'select',
      '#title' =>$this->t('Field for media source'),
      "#empty_option"=>t('- Select -'),
      '#options' => $text_fields,
      '#default_value' => ($config['media_source']) ?? NULL,
      '#description' => $this->t('Field for storing image source'),
      '#states' => [
        'visible' => [
          ':input[name="media"]' =>['!value' => ''],
        ],
      ],
    ];

    $form['media_description'] = [
      '#type' => 'select',
      '#title' =>$this->t('Field for media description'),
      "#empty_option"=>t('- Select -'),
      '#options' => $text_fields,
      '#default_value' => ($config['media_description']) ?? NULL,
      '#description' => $this->t('Field for storing image description'),
      '#states' => [
        'visible' => [
          ':input[name="media"]' =>['!value' => ''],
        ],
      ],
    ];

    $resolution = [
      1 => 'Thumbnail 192px',
      2 => 'Small 420px',
      4 => 'Medium w/ watermark 1000px',
      8 => 'Medium w/o watermark 1000px',
      9 => 'High > 1000px',
    ];
    $form['resolution'] = [
      '#type' => 'select',
      '#title' => $this->t('Picture resolution'),
      "#empty_option"=>t('- Select -'),
      '#options' => $resolution,
      '#default_value' => ($config['resolution']) ?? NULL,
      '#description' => $this->t('Resolution have to correspond to your PHP max upload size.'),
      '#required' => TRUE,
    ];

    $form['cardinality'] = [
      '#type' => 'number',
      '#title' => $this->t('Max number of images download at once'),
      '#min' => 1,
      '#max' => 99,
      '#default_value' => ($config['cardinality']) ?? NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('imago.settings')->getRawData();
    $api = $form_state->getValue('api');
    if (empty($api['api_key'])) {
      $api['api_key'] = $config['api']['api_key'];
    }

    $this->config('imago.settings')
      ->set('api', $api)
      ->set('media_bundle',$form_state->getValue('media_bundle'))
      ->set('image_field', $form_state->getValue('image_field'))
      ->set('imago_id_field', $form_state->getValue('imago_id_field'))
      ->set('media_source', $form_state->getValue('media_source'))
      ->set('media_description', $form_state->getValue('media_description'))
      ->set('width_field', $form_state->getValue('width_field'))
      ->set('height_field', $form_state->getValue('height_field'))
      ->set('cardinality', $form_state->getValue('cardinality'))
      ->set('resolution', $form_state->getValue('resolution'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
