<?php

namespace Drupal\imago\Plugin\views\query;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\State\StateInterface;
use Drupal\imago\ImagoManager;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Imago views query plugin.
 *
 * @ViewsQuery(
 *   id = "imago",
 *   title = @Translation("Imago"),
 *   help = @Translation("Query against the Imago API.")
 * )
 */
class ImagoQueryPluginBase extends QueryPluginBase {

  /**
   * Current user ID.
   *
   * @var int
   */
  protected $currentUser;

  /**
   * Cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The imago manager.
   *
   * @var \Drupal\imago\ImagoManager
   */
  protected $imagoManager;

  /**
   * The current time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user account.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\imago\ImagoManager $imago_manager
   *   The imago manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The current time.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $current_user, CacheBackendInterface $cache, StateInterface $state, ConfigFactoryInterface $config_factory, ImagoManager $imago_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->definition = $plugin_definition + $configuration;
    $this->cache = $cache;
    $this->state = $state;
    $this->configFactory = $config_factory;
    $this->imagoManager = $imago_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('cache.default'),
      $container->get('state'),
      $container->get('config.factory'),
      $container->get('imago.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * Ensure a table exists in the queue.
   *
   * @param string|null $table
   *   The unaliased name of the table to ensure.
   * @param string|null $relationship
   *   The relationship to ensure the table links to.
   *
   * @return string|null
   *   The alias used to refer to this specific table, or NULL if the table
   *   cannot be ensured.
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * Add a field to the query table.
   *
   * This will automatically call ensureTable to make sure the
   * required table exists, *unless* $table is unset.
   *
   * @param string|null $table
   *   The table this field is attached to. If NULL, it is assumed this will
   *   be a formula; otherwise, ensureTable is used to make sure the
   *   table exists.
   * @param string $field
   *   The name of the field to add. This may be a real field or a formula.
   * @param string|null $alias
   *   The alias to create. If not specified, the alias will be $table_$field
   *   unless $table is NULL. When adding formulae, it is recommended that an
   *   alias be used.
   * @param array $params
   *   An array of parameters additional to the field that will control items
   *   such as aggregation functions and DISTINCT. Some values that are
   *   recognized:
   *   - function: An aggregation function to apply, such as SUM.
   *   - aggregate: Set to TRUE to indicate that this value should be
   *     aggregated in a GROUP BY.
   *
   * @return string
   *   The name that this field can be referred to as.
   */
  public function addField($table, $field, $alias = '', array $params = []) {
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {
    $results = [];

    $last_search = 'last_search_' . $this->currentUser->id();
    $key = 'imago.browser.page' . $this->currentUser->id();
    $get_page_number = $this->state->get($key, 0);

    $last_search_results = [];
    if ($get_page_number > 0 && $this->cache->get($last_search)) {
      $last_search_results = $this->cache->get($last_search)->data;
    }
    else {
      $this->cache->delete($last_search);
    }

    $view->old_view = [views_get_current_view()];
    $view->get_total_rows = FALSE;
    $query_config = $this->configFactory->get('imago.settings');
    $config = $query_config->get('api');
    $exposed_data = $view->exposed_data;
    $default_filters = $view->filter;
    $filters = [];

    if (!empty($exposed_data['db'])) {
      $filters['db'] = $exposed_data['db'];
    }
    elseif (!empty($default_filters['db'])) {
      $filters['db'] = $default_filters['db']->value;
    }

    $filters['sortby'] = 'datecreated';
    $filters['sort'] = 'desc';

    if (!empty($exposed_data['caption'])) {
      $filters['querystring'] = $exposed_data['caption'];
      $filters['sortby'] = 'relevanz';
    }
    elseif (!empty($default_filters['caption'])) {
      $search = $default_filters['caption']->value;
      $search = reset($search);
      if (!empty($search)) {
        $filters['querystring'] = $default_filters['caption']->value;
        $filters['sortby'] = 'relevanz';
      }
    }

    $prev_search = FALSE;
    $filters['from'] = 0;

    if ($get_page_number === 0) {
      unset($last_search_results);
    }

    if (isset($exposed_data['limit']) && is_numeric($exposed_data['limit'])) {
      $filters['size'] = (int) $exposed_data['limit'];
    }
    elseif (!empty($default_filters['limit'])) {
      $filters['size'] = (int) $default_filters['limit']->value;
    }

    if (!empty($last_search_results)) {
      if ($last_search_results['keyword'] === $filters['querystring']) {
        $prev_search = TRUE;
        if (is_numeric($last_search_results['limit']) && $last_search_results['limit'] !== $exposed_data['limit']) {
          $prev_search = FALSE;
        }
      }
      $view->total_rows = count($last_search_results['results']);
    }
    if ($prev_search) {
      $filters['from'] = $filters['size'] * $get_page_number;
    }

    $filters['creative'] = 1;
    if (isset($exposed_data['creative'])) {
      $filters['creative'] = (int) $exposed_data['creative'];
    }
    elseif (!empty($default_filters['creative'])) {
      $filters['creative'] = (int) $default_filters['creative']->value;
    }

    $filters['editorial'] = 1;
    if (isset($exposed_data['editorial'])) {
      $filters['editorial'] = (int) $exposed_data['editorial'];
    }
    elseif (!empty($default_filters['editorial'])) {
      $filters['editorial'] = (int) $default_filters['editorial']->value;
    }

    $images = $this->imagoManager->connectSearch($filters);
    if (!empty($last_search_results['results']) && $filters['from'] > 0) {
      $images = array_merge($last_search_results['results'], $images);
    }
    $index = 0;

    $set_state = [];
    // Expire items in 3 days.
    $expire = time() + 259200;
    foreach ($images as $image) {
      $database = $this->getDatabaseId($image['db']);
      $row['caption'] = $image['caption'];
      $row['thumbnail'] = $config['api_url']['base_url'] . '/' . $database . '/' . $image['pictureid'] . '/thumbs';
      $row['pictureid'] = $image['pictureid'];
      $row['source'] = $image['source'];
      $row['datecreated'] = $image['datecreated'];
      $row['archivaldate'] = $image['archivaldate'];
      $row['db'] = $image['db'];
      $row['height'] = $image['height'];
      $row['width'] = $image['width'];
      $row['index'] = $index++;
      $view->result[] = new ResultRow($row);
      $results[$image['pictureid']] = $row;

      $set_state['imago.api_query.' . $image['pictureid']] = [
        'data' => $row,
        'expire' => $expire,
      ];
    }
    if ($set_state) {
      $this->cache->setMultiple($set_state);
    }

    $view_result = $view->result;
    $counted_result = count($view_result);
    $searched = [
      'from' => $counted_result,
      'results' => $results,
      'page' => $view->getCurrentPage(),
    ];
    if (!empty($filters['querystring'])) {
      $searched['keyword'] = $filters['querystring'];
    }
    if (array_key_exists('limit', $exposed_data)) {
      $searched['limit'] = $exposed_data['limit'];
    }
    if (array_key_exists('creative', $exposed_data)) {
      $searched['creative'] = $exposed_data['creative'];
    }
    if (array_key_exists('editorial', $exposed_data)) {
      $searched['editorial'] = $exposed_data['editorial'];
    }
    $view->total_rows = $counted_result;

    $expire = $this->time->getRequestTime() + 600;
    $this->cache->set($last_search, $searched, $expire, ['last_search']);
  }

  /**
   * Add a simple WHERE clause to the query.
   *
   * The caller is responsible for ensuring that all fields are fully
   * qualified (TABLE.FIELD)    * and that the table already exists in
   * the query.
   *
   * The $field, $value and $operator arguments can also be passed in with a
   * single DatabaseCondition object, like this:
   * @code
   * $this->query->addWhere(
   *   $this->options['group'],
   *   (new Condition('OR'))
   *     ->condition($field, $value, 'NOT IN')
   *     ->condition($field, $value, 'IS NULL')
   * );
   * @endcode
   *
   * @param int $group
   *   The WHERE group to add these to; groups are used to create AND/OR
   *   sections. Groups cannot be nested. Use 0 as the default group.
   *   If the group does not yet exist it will be created as an AND group.
   * @param string $field
   *   The name of the field to check.
   * @param null|string $value
   *   The value to test the field against. In most cases, this is a scalar.
   *   For more complex options, it is an array. The meaning of each element
   *   in the array is dependent on the $operator.
   * @param null|string $operator
   *   The comparison operator, such as =, <, or >=. It also accepts more
   *   complex options such as IN, LIKE, LIKE BINARY, or BETWEEN.
   *   Defaults to =. If $field is a string you have to use 'formula' here.
   *
   * @see \Drupal\Core\Database\Query\ConditionInterface::condition()
   * @see \Drupal\Core\Database\Query\Condition
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->setWhereGroup('AND', $group);
    }

    $this->where[$group]['conditions'][] = [
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * Get Imago database id.
   *
   * @param string $database
   *   Database label.
   *
   * @return string
   *   Database id.
   */
  private function getDatabaseId(string $database) {
    $database_id = 'st';
    if ($database === 'sport') {
      $database_id = 'sp';
    }

    return $database_id;
  }

}
