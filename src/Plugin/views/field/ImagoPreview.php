<?php

namespace Drupal\imago\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Class ImagoPreview.
 *
 * @ViewsField("imago_thumbnail")
 */
class ImagoPreview extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $avatar = $this->getValue($values);
    if ($avatar && !empty($values->caption)) {
      return [
        '#theme' => 'imago_thumbnail',
        '#attributes' => [
          'class' => 'imago_thumbnail lazy',
          'alt' => $values->caption,
          'src' => $avatar,
        ],
      ];
    }
  }

}
