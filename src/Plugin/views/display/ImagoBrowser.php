<?php

namespace Drupal\imago\Plugin\views\display;

use Drupal\entity_browser\Plugin\views\display\EntityBrowser;

/**
 * The plugin that handles imago browser display.
 *
 * "imago_browser_display" is a custom property, used with
 * \Drupal\views\Views::getApplicableViews() to retrieve all views with a
 * 'Imago Browser' display.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "imago_browser",
 *   title = @Translation("Imago browser"),
 *   help = @Translation("Displays a view as Imago browser widget."),
 *   theme = "views_view",
 *   admin = @Translation("Imago browser"),
 *   imago_browser_display = TRUE
 * )
 */
class ImagoBrowser extends EntityBrowser {

  /**
   * Handles form elements on a view.
   *
   * @param array $render
   *   Rendered content.
   */
  protected function handleForm(array &$render) {
    if (!empty($this->view->field['imago_browser_select'])) {
      $select = $this->view->field['imago_browser_select'];
      $select->viewsForm($render);

      $render['#post_render'][] = [get_class($this), 'postRender'];
      $substitutions = [];
      foreach ($this->view->result as $row) {
        $form_element_row_id = $select->getRowId($row);

        $substitutions[] = [
          'placeholder' => '<!--form-item-imago_browser_select--' . $form_element_row_id . '-->',
          'field_name' => 'imago_browser_select',
          'row_id' => $form_element_row_id,
        ];
      }

      $render['#substitutions'] = [
        '#type' => 'value',
        '#value' => $substitutions,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $render = ['view' => $this->view->render()];
    $this->handleForm($render);
    return $render;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);
    if (isset($options['use_ajax'])) {
      $options['use_ajax']['value'] = $this->t('Yes (Forced)');
    }
  }

}
