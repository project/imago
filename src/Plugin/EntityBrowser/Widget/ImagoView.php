<?php

namespace Drupal\imago\Plugin\EntityBrowser\Widget;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\entity_browser\WidgetBase;
use Drupal\Core\Url;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\imago\ImagoManager;
use Drupal\views\Views;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Uses a view to provide entity listing in a browser's widget.
 *
 * @EntityBrowserWidget(
 *   id = "imago_view",
 *   label = @Translation("Imago View"),
 *   provider = "views",
 *   description = @Translation("Uses a view to provide entity listing in a browser's widget."),
 *   auto_select = TRUE
 * )
 */
class ImagoView extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The imago manager.
   *
   * @var \Drupal\imago\ImagoManager
   */
  protected $imagoManager;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Catch form submit once.
   *
   * @var bool
   */
  protected $firstSubmit;

  /**
   * Cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'view' => NULL,
        'view_display' => NULL,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('current_user'),
      $container->get('imago.manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('state'),
      $container->get('cache.default')
    );
  }

  /**
   * Constructs a new View object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\imago\ImagoManager $imago_manager
   *   The imago manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The currently active request object.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   State service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, WidgetValidationManager $validation_manager, AccountInterface $current_user, ImagoManager $imago_manager, Request $request, StateInterface $state, CacheBackendInterface $cache) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager);
    $this->currentUser = $current_user;
    $this->imagoManager = $imago_manager;
    $this->request = $request;
    $this->state = $state;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);
    // TODO - do we need better error handling for view and view_display (in
    // case either of those is nonexistent or display not of correct type)?
    $form['#attached']['library'] = ['imago/view'];
    $form['#attached']['drupalSettings']['prev_selected_count'] = 0;

    $key = 'imago.browser.page' . $this->currentUser->id();
    if (isset($form_state->getTriggeringElement()['#name']) && $form_state->getTriggeringElement()['#name'] === 'load_more') {
      $num = $this->state->get($key, 0);
      $new = $num + 1;
      $this->state->set($key, $new);
      $current_page = $new;
      $form['#attached']['drupalSettings']['load_more'] = TRUE;
    }
    else {
      $this->state->delete($key);
      $current_page = 0;
    }

    /** @var \Drupal\views\ViewExecutable $view */
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load($this->configuration['view'])
      ->getExecutable();

    $view->setCurrentPage($current_page);

    $cardinality = (int) \Drupal::config('imago.settings')->get('cardinality');
    $form['#attached']['drupalSettings']['max_cardinality'] = $cardinality;
    $input = $form_state->getUserInput();
    $storage = $form_state->getStorage();
    $prev_selected_count = count($storage['entity_browser']['selected_entities']);
    if (!empty($storage['entity_browser']['selected_entities'])) {
      $form['#attached']['drupalSettings']['selected_images'] = $storage['entity_browser']['selected_entities'];
      $form['#attached']['drupalSettings']['prev_selected_count'] = $prev_selected_count;
      $form['#attached']['drupalSettings']['prev_search'] = $input['caption'];
    }

    if (!empty($input['imago_browser_select']) || !empty($input['selected_images'])) {
      if (!empty($input['selected_images'])) {
        $unique = array_unique(array_merge($input['selected_images'], $input['imago_browser_select']));
        $form['#attached']['drupalSettings']['selected_images'] = $unique;
      }
      else {
        $form['#attached']['drupalSettings']['selected_images'] = $input['imago_browser_select'];
      }
    }
    $exposed_input = [];
    $exposed_input['db'] = (!empty($input['db']) ? $input['db'] : 'st');
    if (!empty($input['caption'])) {
      $exposed_input['caption'] = $input['caption'];
    }
    if (!empty($input['creative'])) {
      $exposed_input['creative'] = $input['creative'];
    }
    if (!empty($input['editorial'])) {
      $exposed_input['editorial'] = $input['editorial'];
    }
    if (!empty($input['limit'])) {
      $exposed_input['limit'] = $input['limit'];
    }
    $view->setExposedInput($exposed_input);
    $view->result = [];
    $form_state->setRebuild();

    $tags = $view->getCacheTags();
    foreach ($tags as $tag) {
      $this->cache->invalidate($tag);
    }

    if (!empty($this->configuration['arguments'])) {
      if (!empty($additional_widget_parameters['path_parts'])) {
        $arguments = [];
        // Map configuration arguments with original path parts.
        foreach ($this->configuration['arguments'] as $argument) {
          $arguments[] = isset($additional_widget_parameters['path_parts'][$argument]) ? $additional_widget_parameters['path_parts'][$argument] : '';
        }
        $view->setArguments(array_values($arguments));
      }
    }

    $form['view'] = $view->executeDisplay($this->configuration['view_display']);

    if (empty($view->field['imago_browser_select'])) {
      $url = Url::fromRoute('entity.view.edit_form', ['view' => $this->configuration['view']])->toString();
      if ($this->currentUser->hasPermission('administer views')) {
        return [
          '#markup' => $this->t('Imago browser select form field not found on a view. <a href=":link">Go fix it</a>!', [':link' => $url]),
        ];
      }
      else {
        return [
          '#markup' => $this->t('Imago browser select form field not found on a view. Go fix it!'),
        ];
      }
    }

    // When rebuilding makes no sense to keep checkboxes that were previously
    // selected.
    if (!empty($form['view']['imago_browser_select'])) {
      foreach (Element::children($form['view']['imago_browser_select']) as $child) {
        $form['view']['imago_browser_select'][$child]['#process'][] = ['\Drupal\imago\Plugin\EntityBrowser\Widget\ImagoView', 'processCheckbox'];
        $form['view']['imago_browser_select'][$child]['#process'][] = ['\Drupal\Core\Render\Element\Checkbox', 'processAjaxForm'];
        $form['view']['imago_browser_select'][$child]['#process'][] = ['\Drupal\Core\Render\Element\Checkbox', 'processGroup'];
      }
    }


    $form['actions']['submit']['#attributes']['class'][] = 'imago_browser';

    $form['load_more'] = [
      '#name' => 'load_more',
      '#type' => 'submit',
      '#value' => $this->t('Load more images'),
      '#attributes' => ['class' => ['imago-load-more', 'page-loaded-' . $current_page]],
      '#current_page' => $current_page,
    ];

    $form['view']['view'] = [
      '#markup' => \Drupal::service('renderer')->render($form['view']['view']),
    ];

    return $form;
  }

  /**
   * Sets the #checked property when rebuilding form.
   *
   * Every time when we rebuild we want all checkboxes to be unchecked.
   *
   * @see \Drupal\Core\Render\Element\Checkbox::processCheckbox()
   */
  public static function processCheckbox(&$element, FormStateInterface $form_state, &$complete_form) {
    if ($form_state->isRebuilding()) {
      $element['#checked'] = FALSE;
    }

    return $element;
  }

  /**
   * Get Imago browser tab.
   *
   * @param array $form
   *   Entity browser form.
   * @param \CDrupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return int|string
   *   Key of tab.
   */
  private function getImageBrowserTab(array &$form, FormStateInterface $form_state) {
    $entity_browser = $form_state->getFormObject()->getEntityBrowser();
    $widgets = $entity_browser->getWidgets();
    foreach ($widgets->getConfiguration() as $key => $tab) {
      if (strpos($tab['id'], 'imago') !== FALSE) {
        return $key;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    $selected_rows = array_values(array_filter($form_state->getUserInput()['imago_browser_select']));
    $entities = [];
    foreach ($selected_rows as $row) {
      [$type, $imago_id] = explode(':', $row);
      $entity = $this->imagoManager->getEntity($imago_id);
      if (is_null($entity) || !$entity) {
        $entity = $this->imagoManager->download($imago_id);
      }
      if ($entity instanceof EntityInterface) {
        $entities[$imago_id] = $entity;
      }
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $input = $form_state->getUserInput();
    $this->firstSubmit = TRUE;
    if (!empty($input['imago_browser_select'])) {
      $form_state->setUserInput($input);
      $storage = $form_state->getStorage();
      $storage['entity_browser']['selected_entities'] = [] ;
      $form_state->setStorage($storage);
      $entities = $this->prepareEntities($form, $form_state);
      $this->selectEntities($entities, $form_state);
      $this->firstSubmit = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $options = [];
    // Get only those enabled Views that have entity_browser displays.
    $displays = Views::getApplicableViews('imago_browser_display');
    foreach ($displays as $display) {
      [$view_id, $display_id] = $display;
      $view = $this->entityTypeManager->getStorage('view')->load($view_id);
      $options[$view_id . '.' . $display_id] = $this->t('@view : @display', ['@view' => $view->label(), '@display' => $view->get('display')[$display_id]['display_title']]);
    }

    $form['view'] = [
      '#type' => 'select',
      '#title' => $this->t('Imago : View display'),
      '#default_value' => $this->configuration['view'] . '.' . $this->configuration['view_display'],
      '#options' => $options,
      '#empty_option' => $this->t('- Select a view -'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Run widget validators.
   *
   * @param array $entities
   *   Array of entity ids to validate.
   * @param array $validators
   *   Array of widget validator ids.
   *
   * @return \Symfony\Component\Validator\ConstraintViolationListInterface
   *   A list of constraint violations. If the list is empty, validation
   *   succeeded.
   */
  protected function runWidgetValidators(array $entities, $validators = []) {
    return new ConstraintViolationList();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues()['table'][$this->uuid()]['form'];
    $this->configuration['submit_text'] = $values['submit_text'];
    $this->configuration['auto_select'] = $values['auto_select'];
    if (!empty($values['view'])) {
      list($view_id, $display_id) = explode('.', $values['view']);
      $this->configuration['view'] = $view_id;
      $this->configuration['view_display'] = $display_id;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    // Mark the widget as not visible if the user has no access to the view.
    /** @var \Drupal\views\ViewExecutable $view */
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load($this->configuration['view'])
      ->getExecutable();

    // Check if the current user has access to this view.
    return AccessResult::allowedIf($view->access($this->configuration['view_display']));
  }

}
