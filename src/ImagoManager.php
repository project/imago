<?php

namespace Drupal\imago;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file\FileRepository;
use Drupal\file\FileRepositoryInterface;
use Drupal\media\Entity\Media;
use Drupal\token\Token;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

/**
 * Fetch and save data from Imago.
 */
class ImagoManager {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * Media storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $mediaStorage;

  /**
   * Field config storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $fieldConfigStorage;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Service to retrieve token information.
   *
   * @var \Drupal\token\Token
   */
  protected $token;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Settings for stream wrappers.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * Cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Image module configuration.
   *
   * @var array
   */
  protected $imagoConfig;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\ultimate_cron\Logger\LoggerInterface
   */
  protected $logger;

  /**
   * File repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * Constructs a EntityCreateAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user account.
   * @param \Drupal\token\Token $token
   *   Service to retrieve token information.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   FileSystemInterface service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   State service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\ultimate_cron\Logger\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_manager,
    AccountProxyInterface $current_user,
    Token $token,
    FileSystem $file_system,
    FileRepositoryInterface $file_repository,
    CacheBackendInterface $cache,
    ConfigFactoryInterface $config_factory,
    MessengerInterface $messenger,
    LoggerChannelInterface $logger
  ) {
    $this->entityTypeManager = $entity_manager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->mediaStorage = $this->entityTypeManager->getStorage('media');
    $this->fieldConfigStorage = $this->entityTypeManager->getStorage('field_config');
    $this->currentUser = $current_user;
    $this->token = $token;
    $this->fileSystem = $file_system;
    $this->cache = $cache;
    $this->imagoConfig = $config_factory->get('imago.settings')->getRawData();
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->fileRepository = $file_repository;
  }

  /**
   * Fetch data from Image Search API.
   *
   * @return array
   * @throws \Exception
   */
  public function connectSearch($filters = []) {

    $hash = md5(serialize($filters));
    $cached_data = $this->cache->get('imago.api.' . $hash);
    if ($cached_data && isset($cached_data->data) && is_array($cached_data->data)) {
      return $cached_data->data;
    }

    $time = time();
    $timeout = 30;
    $config = $this->imagoConfig['api'];
    $content_uri = $config['api_url']['search_url'];
    $options = [
      'headers' => [
        'x-api-user' => $config['api_user'],
        'x-api-key' => $config['api_key'],
      ],
      'body' => json_encode($filters),
      'timeout' => $timeout,
    ];
    $response = $this->getImagoData($content_uri, $options);
    $final_time = time() - $time;
    if ($response) {
      if ($final_time > 8) {
        $this->messenger->addWarning('Imago library connection took ' . $final_time . 'sec. Performance degradation may occur.');
      }
      $data = json_decode($response->getBody()->getContents(), TRUE);
      if (!empty($data) && is_array($data) && isset($data[1]['pictures'])) {
        $data = $data[1]['pictures'];
        $this->cache->set('imago.api.' . $hash, $data, $time + 180);
        return $data;
      }
    }
    else {
      $this->messenger->addError('Cannot connect to Imago library in time (' . $timeout . 'sec). Please try again later.');
    }

    return [];
  }

  /**
   * Download image from Imago by imago id.
   *
   * @param int $imago_id
   *   Image-imago.de image id.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface
   *   Media entity.
   */
  public function download(int $imago_id, $fallback_resolution = NULL) {
    $image_data = $this->cache->get('imago.api_query.' . $imago_id);
    if ($image_data && isset($image_data->data)) {
      $image_data = $image_data->data;
    }
    else {
      $api_results = $this->connectSearch(['pictureid' => $imago_id]);
      $image_data = reset($api_results);
    }
    $resolution = (int) $this->imagoConfig['resolution'] ?? 8;
    if (!is_null($fallback_resolution)) {
      $resolution = $fallback_resolution;
    }

    $config = $this->imagoConfig['api'];
    $filters = [
      "pictureid" => $imago_id,
      "db" => $image_data['db'],
      "res" => $resolution,
    ];
    $content_uri = $config['api_url']['download_url'];
    $options = [
      'headers' => [
        'x-api-user' => $config['api_user'],
        'x-api-key' => $config['api_key'],
      ],
      'body' => json_encode($filters),
    ];

    $response = $this->getImagoData($content_uri, $options);
    if ($response instanceof Response) {
      $stream = $response->getBody()->detach();
      return $this->createImageEntity($imago_id, $stream);
    }
    elseif ($resolution !== $fallback_resolution) {
      return $this->download($imago_id, 8);
    }

    return FALSE;
  }

  /**
   * Get imago data and validate response.
   *
   * @param string $url
   *   Requested url.
   * @param array $options
   *   Request options.
   *
   * @return bool|array
   *   Array with JSON data, or FALSE.
   *
   * @throws \Exception
   */
  private function getImagoData(string $url, array $options = []) {
    try {
      /** @var \GuzzleHttp\Client $client */
      $client = new Client();
      return $client->post($url, $options);
    }
    catch (\Exception $e) {
      watchdog_exception('imago', $e, $e->getMessage());
      return FALSE;
    }
  }

  /**
   * Verify if image exist in our system, or create new one.
   *
   * @param int $imago_id
   *   Imago id.
   * @param string $bundle
   *   Media bundle.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface
   *   Media entity.
   */
  public function getEntity(int $imago_id, string $entity_type = 'media') {
    $properties = [];
    if ($entity_type === 'file') {
      $properties['filename'] = 'imago_' . $imago_id . '.jpg';
    }
    elseif ($entity_type === 'media') {
      $properties['name'] = 'imago_' . $imago_id . '.jpg';
    }
    $existing_entities = $this->entityTypeManager->getStorage($entity_type)->loadByProperties($properties);
    if (!empty($existing_entities)) {
      return reset($existing_entities);
    }

    return NULL;
  }

  /**
   * Create image entity.
   *
   * @param int $imago_id
   *   Source image data.
   * @param resource $stream
   *   Source image stream.
   * @param string $bundle
   *   Media bundle for creating image.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface
   *   Media entity if image was created.
   *   False if image can't be created.
   *
   * @throws \Exception
   */
  private function createImageEntity(int $imago_id, $stream) {
    $data = $this->cache->get('imago.api_query.' . $imago_id);
    if ($data && isset($data->data) && is_array($data->data)) {
      $data = $data->data;
    }
    else {
      return FALSE;
    }

    $image_properties = [
      'name' => 'imago_' . $imago_id . '.jpg',
      'bundle' => $this->imagoConfig['media_bundle'],
      'uid' => $this->currentUser->id(),
      'pictureid' => $imago_id,
      'created' => $data['datecreated'],
    ];

    $file_id = $this->saveToStorage($stream, $imago_id);
    if (!empty($this->imagoConfig['image_field']) && $file_id) {
      $image_properties[$this->imagoConfig['image_field']] = [
        'target_id' => $file_id,
        'alt' => substr($data['caption'], 0, 512),
        'width' => $data['width'],
        'height' => $data['height'],
      ];
    }
    if (!empty($this->imagoConfig['media_source'])) {
      $image_properties[$this->imagoConfig['media_source']] = $data['source'];
    }
    if (!empty($this->imagoConfig['media_description'])) {
      $image_properties[$this->imagoConfig['media_description']] = [
        'value' => substr($data['caption'], 0, 512),
        'format' => 'plain',
      ];
    }
    if (!empty($this->imagoConfig['height_field'])) {
      $image_properties[$this->imagoConfig['height_field']] = $data['height'];
    }
    if (!empty($this->imagoConfig['width_field'])) {
      $image_properties[$this->imagoConfig['width_field']] = $data['width'];
    }
    if (!empty($this->imagoConfig['imago_id_field'])) {
      $image_properties[$this->imagoConfig['imago_id_field']] = $imago_id;
    }

    $image_media = Media::create($image_properties);
    if ($image_media->save() === SAVED_NEW) {
      return $image_media;
    }

    return FALSE;
  }

  /**
   * Save image to storage.
   *
   * @param int $imago_id
   *   Image-imago.de image id.
   * @param string $bundle
   *   Media bundle.
   * @param string $image_field
   *   Image field name.
   * @param resource $stream
   *   Data stream.
   *
   * @return int|false
   *   File ID or FALSE in case of error.
   *
   * @throws \Exception
   */
  private function saveToStorage($stream, int $imago_id) {
    $existing_file = $this->getEntity($imago_id, 'file');
    if ($existing_file instanceof FileInterface) {
      return $existing_file->id();
    }
    $field_instance = $this->fieldConfigStorage->load('media.' . $this->imagoConfig['media_bundle'] . '.' . $this->imagoConfig['image_field']);
    $file_directory = $this->token->replace($field_instance->getSetting('file_directory'));
    $uri_scheme = $field_instance->getSetting('uri_scheme');
    $directory = $uri_scheme . '://' . $file_directory . '/';
    $exist = $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    if ($exist) {
      $destination = $uri_scheme . '://' . $file_directory . '/' . $imago_id . '.jpg';
      if (file_exists($destination)) {
        $files = $this->entityTypeManager->getStorage('file')->loadByProperties(['uri' => $destination]);
        $file = reset($files);
      }
      if (!isset($file) || !$file instanceof FileInterface) {
          $uri = $this->fileSystem->saveData($stream, $destination);
          $file = File::create([
              'uri' => $uri,
              'uid' => $this->currentUser->id(),
          ]);
          $file->setPermanent();
      }

      $file->setFilename('imago_' . $imago_id . '.jpg');
      $save = $file->save();
      if ($file && ($save === SAVED_NEW || $save === SAVED_UPDATED)) {
        return $file->id();
      }
      else {
        throw new \Exception(t("File can't be saved."));
      }
    }
    return FALSE;
  }

}
